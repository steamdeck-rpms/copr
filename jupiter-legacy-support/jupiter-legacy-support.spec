Name:		 	jupiter-legacy-support
Version:		1.138
Release:		1%{?dist}
Summary:		"Legacy jupiter-specific support files that haven't been split to their own package or removed."

License:		MPL 2.0
URL:			http://repo.steampowered.com
Source0:		https://gitlab.com/evlaV/jupiter-PKGBUILD/-/archive/master/jupiter-PKGBUILD-master.tar.gz

BuildRequires:  curl tar
Requires:		python3-psutil python3-aiohttp nvme-cli flatpak
Supplements:	jupiter-hw-support

Provides:		steam-web-debug-portforward.service = %{version}-%{release}
Provides:		91-dracut-install-vanilla.hook = %{version}-%{release}
Provides:		killuserprocesses.conf = %{version}-%{release}
Provides:		flathub-beta.flatpakrepo = %{version}-%{release}
Provides:		flatpak-workaround.service = %{version}-%{release}
Provides:		flatpak-modify-flathub-beta.service = %{version}-%{release}
Provides:		steamos-prepare-oobe-test = %{version}-%{release}
Provides:		sudoers.d-wheel-prepare-oobe-test = %{version}-%{release}
Provides:		org.valve.steamos.%{name}.policy = %{version}-%{release}
Provides:		black_800x1280.png = %{version}-%{release}
Provides:		white_800x1280.png = %{version}-%{release}

%description
%{summary}

%prep
curl --location --remote-time --show-error --fail --output %{SOURCE0} https://gitlab.com/evlaV/jupiter-PKGBUILD/-/archive/master/jupiter-PKGBUILD-master.tar.gz

%setup -q -c -n jupiter-PKGBUILD-master

%install
mkdir -p %{buildroot}/usr/share/jupiter_testing/
install -m 0644 jupiter-PKGBUILD-master/%{name}/black_800x1280.png %{buildroot}/usr/share/jupiter_testing/black_800x1280.png
install -m 0644 jupiter-PKGBUILD-master/%{name}/white_800x1280.png %{buildroot}/usr/share/jupiter_testing/white_800x1280.png

mkdir -p %{buildroot}/etc/flatpak/remotes.d/
install -m 0644 jupiter-PKGBUILD-master/%{name}/flathub-beta.flatpakrepo %{buildroot}/etc/flatpak/remotes.d/flathub-beta.flatpakrepo

mkdir -p %{buildroot}/usr/lib/systemd/system/
install -m 0644 jupiter-PKGBUILD-master/%{name}/flatpak-workaround.service %{buildroot}/usr/lib/systemd/system/flatpak-workaround.service
install -m 0644 jupiter-PKGBUILD-master/%{name}/flatpak-modify-flathub-beta.service %{buildroot}/usr/lib/systemd/system/flatpak-modify-flathub-beta.service

mkdir -p %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/
ln -sv /usr/lib/systemd/system/flatpak-workaround.service %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/
ln -sv /usr/lib/systemd/system/flatpak-modify-flathub-beta.service %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/

mkdir -p %{buildroot}/usr/bin/
install -m 0755 jupiter-PKGBUILD-master/%{name}/steamos-prepare-oobe-test %{buildroot}/usr/bin/steamos-prepare-oobe-test

mkdir -p %{buildroot}/etc/sudoers.d/
install -m 0440 jupiter-PKGBUILD-master/%{name}/sudoers.d-wheel-prepare-oobe-test %{buildroot}/etc/sudoers.d/wheel-prepare-oobe-test

mkdir -p %{buildroot}/usr/share/polkit-1/actions/
install -m 0644 jupiter-PKGBUILD-master/%{name}/org.valve.steamos.jupiter-legacy-support.policy %{buildroot}/usr/share/polkit-1/actions/org.valve.steamos.jupiter-legacy-support.policy

mkdir -p  %{buildroot}/etc/systemd/logind.conf.d/
install -m 0644 jupiter-PKGBUILD-master/%{name}/killuserprocesses.conf %{buildroot}/etc/systemd/logind.conf.d/killuserprocesses.conf

mkdir -p  %{buildroot}/usr/share/libalpm/hooks/
install -m 0644 jupiter-PKGBUILD-master/%{name}/91-dracut-install-vanilla.hook %{buildroot}/usr/share/libalpm/hooks/91-dracut-install-vanilla.hook

mkdir -p %{buildroot}/etc/systemd/system/
install -m 0644 jupiter-PKGBUILD-master/%{name}/steam-web-debug-portforward.service %{buildroot}/etc/systemd/system/steam-web-debug-portforward.service

%files
%defattr(-,root,root,-)
/usr/lib/systemd/system/flatpak-workaround.service
/usr/lib/systemd/system/flatpak-modify-flathub-beta.service
/usr/bin/steamos-prepare-oobe-test
/usr/share/jupiter_testing/black_800x1280.png
/usr/share/jupiter_testing/white_800x1280.png
/usr/share/polkit-1/actions/org.valve.steamos.jupiter-legacy-support.policy
/usr/share/libalpm/hooks/91-dracut-install-vanilla.hook
/etc/flatpak/remotes.d/flathub-beta.flatpakrepo
/etc/sudoers.d/wheel-prepare-oobe-test
/etc/systemd/logind.conf.d/killuserprocesses.conf
/etc/systemd/system/steam-web-debug-portforward.service
/usr/lib/systemd/system/multi-user.target.wants/flatpak-workaround.service
/usr/lib/systemd/system/multi-user.target.wants/flatpak-modify-flathub-beta.service

%changelog
* Tue Dec 20 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Update to v1.138
- https://gitlab.com/evlaV/jupiter-PKGBUILD/-/commit/204aeea57833deac537979bb66cb18b3855f5680

* Mon Dec 05 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Update to v1.136
- https://gitlab.com/evlaV/jupiter-PKGBUILD/-/commit/bbcbe99ab5e5b0423c37f80ec213f2d43452c1f9

* Mon Nov 07 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build based on Arch PKGBUILD sources
