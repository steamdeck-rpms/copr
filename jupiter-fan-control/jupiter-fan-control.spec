Name:			jupiter-fan-control
Version:		20230209.1
Release:		1%{?dist}
Summary:		"Jupiter fan controller."

License:		MIT
URL:			http://repo.steampowered.com
Source0:		https://gitlab.com/evlaV/jupiter-fan-control/-/archive/main/jupiter-fan-control-main.tar.gz
Patch0:			%{name}.patch

BuildRequires:  curl tar
Requires:		python3-unidecode

Provides:		%{name}.service = %{version}-%{release}
Provides:		%{name}.service = %{version}-%{release}
Provides:		PID.py = %{version}-%{release}
Provides:		fancontrol.py = %{version}-%{release}
Provides:		%{name}-config.yaml = %{version}-%{release}

%description
%{summary}

%prep
curl --location --remote-time --show-error --fail --output %{SOURCE0} https://gitlab.com/evlaV/jupiter-fan-control/-/archive/main/jupiter-fan-control-main.tar.gz
curl --location --remote-time --show-error --fail --output %{PATCH0} https://gitlab.com/steamdeck-rpms/copr/-/raw/main/jupiter-fan-control/jupiter-fan-control.patch

%setup -q -c -n %{name}-main

%patch0 -p0

%install
mkdir -p %{buildroot}/usr/lib/systemd/system/
install -m 0644 %{name}-main/usr/lib/systemd/system/%{name}.service %{buildroot}/usr/lib/systemd/system/%{name}.service

mkdir -p %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/
ln -sv /usr/lib/systemd/system/%{name}.service %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/

mkdir -p %{buildroot}/usr/share/%{name}/
install -m 0755 %{name}-main/usr/share/%{name}/PID.py %{buildroot}/usr/share/%{name}/PID.py
install -m 0755 %{name}-main/usr/share/%{name}/fancontrol.py %{buildroot}/usr/share/%{name}/fancontrol.py
install -m 0644 %{name}-main/usr/share/%{name}/%{name}-config.yaml %{buildroot}/usr/share/%{name}/%{name}-config.yaml

%files
%defattr(-,root,root,-)
/usr/lib/systemd/system/%{name}.service
/usr/lib/systemd/system/multi-user.target.wants/%{name}.service
/usr/share/%{name}/PID.py
/usr/share/%{name}/fancontrol.py
/usr/share/%{name}/%{name}-config.yaml

%changelog
* Sun Feb 12 2023 Karlis Kavacis karlis.kavacis@protonmail.com
- Update to 20230209.1

* Mon Nov 07 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build based on Arch PKGBUILD sources
