%global debug_package %{nil}

Name:			rtl88x2ce-dkms
Version:		5.12.1.6
Release:		1%{?dist}
Summary:		"Realtek RTL88x2CE Driver (DKMS)"

License:		GPL
URL:			http://repo.steampowered.com
Source0:		https://gitlab.com/evlaV/jupiter-PKGBUILD/-/archive/master/jupiter-PKGBUILD-master.tar.gz

ExclusiveArch:	x86_64
BuildRequires:	curl tar
Requires:		dkms kernel-devel

Provides:		rtl88x2ce = %{version}-%{release}
Provides:		rtw88_blacklist.conf = %{version}-%{release}
Provides:		rtl88x2ce.conf = %{version}-%{release}

%description
%{summary}

%prep
curl --location --fail --silent --output %{SOURCE0} https://gitlab.com/evlaV/jupiter-PKGBUILD/-/archive/master/jupiter-PKGBUILD-master.tar.gz

%setup -q -c -n jupiter-PKGBUILD-master

%install
mkdir -p %{buildroot}/usr/src/%{name}-%{version}
sed -i "s/GRO_DROP/GRO_MERGED_FREE/" jupiter-PKGBUILD-master/os_dep/linux/recv_linux.c
cp -pr jupiter-PKGBUILD-master/* %{buildroot}/usr/src/%{name}-%{version}
install -m 0644 jupiter-PKGBUILD-master/dkms.conf %{buildroot}/usr/src/%{name}-%{version}/dkms.conf
install -m 0644 jupiter-PKGBUILD-master/rtw88_blacklist.conf %{buildroot}/usr/lib/modprobe.d/rtw88_blacklist.conf
printf '%s\n' 'rtl88x2ce' > %{buildroot}/etc/modules-load.d/rtl88x2ce.conf
sed -e "s/#MODULE_VERSION#/%{version}/" -i %{buildroot}/usr/src/%{name}-%{version}/dkms.conf

%post
if ! /usr/sbin/dkms status %{module}/%{version} | grep -q added
then
    /usr/sbin/dkms add -m %{module} -v %{version}
fi
/usr/sbin/dkms build -m %{module} -v %{version}
/usr/sbin/dkms install -m %{module} -v %{version}
exit 0

%preun
/usr/sbin/dkms remove -m %{module} -v %{version} --all
exit 0

%changelog
* Sun Feb 12 2023 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build
