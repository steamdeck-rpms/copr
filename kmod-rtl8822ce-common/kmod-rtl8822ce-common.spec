%global debug_package %{nil}
%global prjname rtl8822ce

Name:           kmod-rtl8822ce-common
Version:        Master
Release:        1%{?dist}
Summary:        "Common file for rtl8822ce kernel module."

License:        GNU
URL:            https://github.com/lwfinger/rtw88
Source0:        https://gitlab.com/steamdeck-rpms/copr/-/archive/main/copr-main.tar.gz

Requires:       kmod-rtl8822ce
Provides:       rtl8822ce-kmod-common = %{version}-%{release}
Provides:       rtw88_blacklist.conf = %{version}-%{release}

%description
%{summary}

%prep
curl --location --remote-time --show-error --fail --output %{SOURCE0} https://gitlab.com/steamdeck-rpms/copr/-/archive/main/copr-main.tar.gz

%setup -q -c -n copr-main

%install
mkdir -p %{buildroot}/usr/lib/modprobe.d/
install -m 0644 copr-main/kmod-%{prjname}-common/rtw88_blacklist.conf %{buildroot}/usr/lib/modprobe.d/rtw88_blacklist.conf

%files
%defattr(-,root,root,-)
/usr/lib/modprobe.d/rtw88_blacklist.conf

%changelog
* Mon Nov 28 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build based on rtw88 sources by lwfinger
