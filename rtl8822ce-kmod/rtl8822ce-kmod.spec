%if 0%{?fedora}
%global buildforkernels akmod
%endif

%global debug_package %{nil}
%global prjname rtl8822ce

Name:           rtl8822ce-kmod
Version:        Master
Release:        1%{?dist}
Summary:        "Realtek RTL8822CE Drivers from rtw88 Git repo"

License:        GNU
URL:            https://github.com/lwfinger/rtw88
Source0:        https://github.com/lwfinger/rtw88/archive/refs/heads/master.tar.gz

ExclusiveArch:  x86_64
BuildRequires:  kmodtool
%{!?kernels:BuildRequires: buildsys-build-rpmfusion-kerneldevpkgs-%{?buildforkernels:%{buildforkernels}}%{!?buildforkernels:current}-%{_target_cpu}}

# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo rpmfusion --kmodname %{prjname} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null) }

%description
%{summary}

%prep
curl --location --fail --silent --output %{SOURCE0} https://github.com/lwfinger/rtw88/archive/refs/heads/master.tar.gz

# Error out if there was something wrong with kmodtool
%{?kmodtool_check}

# Print kmodtool output for debugging purposes:
kmodtool --target %{_target_cpu} --repo rpmfusion --kmodname %{prjname} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null

for kernel_version in %{?kernel_versions} ; do
    cp -a rtw88-main _kmod_build_${kernel_version%%___*}
done

%build
for kernel_version in %{?kernel_versions}; do
    %make_build -C ${kernel_version##*___} modules M=$PWD/_kmod_build_${kernel_version%%___*}
done

%install
for kernel_version in %{?kernel_versions}; do
    mkdir -p %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
    install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_8822ce.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
    install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_core.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
    install -D -m 755 _kmod_build_${kernel_version%%___*}/rtw_pci.ko %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/
    chmod a+x %{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/*.ko
done
%{?akmod_install}

%changelog
* Mon Nov 28 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build based on rtw88 sources by lwfinger
