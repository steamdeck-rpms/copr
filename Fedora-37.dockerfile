#Download base image Fedora 37
FROM registry.fedoraproject.org/fedora:37

#Necessary build tools
RUN dnf makecache
RUN dnf install git curl rpm-build rpm-devel rpmlint rpmdevtools gcc make mock -y
RUN adduser builder; usermod -aG mock builder; su - builder -c rpmdev-setuptree
