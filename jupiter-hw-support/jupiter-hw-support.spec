%global __brp_check_rpaths %{nil}
# Broken RPATH check disabled because source is in binary format and thus cannot be patched
# https://fedoraproject.org/wiki/Changes/Broken_RPATH_will_fail_rpmbuild#Opting_out

Name:			jupiter-hw-support
Version:		20230201
Release:		1%{?dist}
Summary:		"Jupiter HW support package."

License:		MIT
URL:			http://repo.steampowered.com
Source0:		https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.tar.gz
Patch0:			%{name}.patch

BuildRequires:  curl tar xcursorgen
Requires:		python3-evdev dmidecode python3-crcmod python3-click python3-progressbar2 python3-hidapi jq alsa-utils parted e2fsprogs jlink nvme-cli flatpak dejavu-sans-fonts
Supplements:	jupiter-legacy-support

Provides:		grub-steamos = %{version}-%{release}
Provides:		alsa-restore.service = %{version}-%{release}
Provides:		steamos-automount@.service = %{version}-%{release}
Provides:		jupiter-biosupdate.service = %{version}-%{release}
Provides:		jupiter-controller-update.service = %{version}-%{release}
Provides:		kded5rc = %{version}-%{release}
Provides:		amd_system_info = %{version}-%{release}
Provides:		foxnet-biosupdate = %{version}-%{release}
Provides:		jupiter-biosupdate = %{version}-%{release}
Provides:		jupiter-check-support = %{version}-%{release}
Provides:		jupiter-controller-update = %{version}-%{release}
Provides:		thumbstick_cal = %{version}-%{release}
Provides:		thumbstick_fine_cal = %{version}-%{release}
Provides:		trigger_cal = %{version}-%{release}
Provides:		jupiter-amp-control = %{version}-%{release}
Provides:		jupiter-biosupdate = %{version}-%{release}
Provides:		jupiter-check-support = %{version}-%{release}
Provides:		jupiter-dock-updater = %{version}-%{release}
Provides:		jupiter-fan-control = %{version}-%{release}
Provides:		jupiter-get-als-gain = %{version}-%{release}
Provides:		steamos-devkit-mode = %{version}-%{release}
Provides:		steamos-disable-wireless-power-management = %{version}-%{release}
Provides:		steamos-enable-sshd = %{version}-%{release}
Provides:		steamos-factory-reset-config = %{version}-%{release}
Provides:		steamos-format-device = %{version}-%{release}
Provides:		steamos-format-sdcard = %{version}-%{release}
Provides:		steamos-poweroff-now = %{version}-%{release}
Provides:		steamos-priv-write = %{version}-%{release}
Provides:		steamos-reboot-now = %{version}-%{release}
Provides:		steamos-reboot-other = %{version}-%{release}
Provides:		steamos-restart-sddm = %{version}-%{release}
Provides:		steamos-select-branch = %{version}-%{release}
Provides:		steamos-set-hostname = %{version}-%{release}
Provides:		steamos-set-timezone = %{version}-%{release}
Provides:		steamos-trim-devices = %{version}-%{release}
Provides:		steamos-update = %{version}-%{release}
Provides:		cirrus-fixup.sh = %{version}-%{release}
Provides:		cs35l41-dsp1-spk-prot.bin.mod = %{version}-%{release}
Provides:		cs35l41-dsp1-spk-prot.bin.orig = %{version}-%{release}
Provides:		ev2_cirrus_alsa_fixups.sh = %{version}-%{release}
Provides:		format-device.sh = %{version}-%{release}
Provides:		format-sdcard.sh = %{version}-%{release}
Provides:		jupiter-amp-control = %{version}-%{release}
Provides:		power-button-handler.py = %{version}-%{release}
Provides:		steamos-automount.sh = %{version}-%{release}
Provides:		trim-devices.sh = %{version}-%{release}
Provides:		80-gpu-reset.rules = %{version}-%{release}
Provides:		99-power-button.rules = %{version}-%{release}
Provides:		99-steamos-automount.rules = %{version}-%{release}
Provides:		HiFi-upstream.conf = %{version}-%{release}
Provides:		HiFi.conf = %{version}-%{release}
Provides:		acp5x.conf = %{version}-%{release}
Provides:		arrow = %{version}-%{release}
Provides:		left_ptr = %{version}-%{release}
Provides:		left_ptr_help = %{version}-%{release}
Provides:		left_ptr_watch = %{version}-%{release}
Provides:		index.theme = %{version}-%{release}
Provides:		F7A0113_sign.fd = %{version}-%{release}
Provides:		BatCtrl = %{version}-%{release}
Provides:		H2OFFTx64-G.sh = %{version}-%{release}
Provides:		H2OFFTx64.sh = %{version}-%{release}
Provides:		Logo.png = %{version}-%{release}
Provides:		phy_alloc.ko = %{version}-%{release}
Provides:		h2offt = %{version}-%{release}
Provides:		h2offt-g = %{version}-%{release}
Provides:		h2osde-lx64 = %{version}-%{release}
Provides:		msg_cht.ini = %{version}-%{release}
Provides:		msg_eng.ini = %{version}-%{release}
Provides:		platform.ini = %{version}-%{release}
Provides:		D20_APP_REL_63D05208.bin = %{version}-%{release}
Provides:		D21_APP_REL_63D051FB.bin = %{version}-%{release}
Provides:		RA_APP_REL_63D05207.bin = %{version}-%{release}
Provides:		EV2_REL_62C853C3.bin = %{version}-%{release}
Provides:		boot_ra_Release.srec = %{version}-%{release}
Provides:		BatCtrl = %{version}-%{release}
Provides:		Devices.xml = %{version}-%{release}
Provides:		License_Agreement.txt = %{version}-%{release}
Provides:		Messages.xml = %{version}-%{release}
Provides:		libRFP.so = %{version}-%{release}
Provides:		rfp-cli = %{version}-%{release}
Provides:		rfp_cli_linux.sh = %{version}-%{release}
Provides:		d20bootloader.py = %{version}-%{release}
Provides:		d21bootloader16.py = %{version}-%{release}
Provides:		steamos.plymouth = %{version}-%{release}
Provides:		steamos.png = %{version}-%{release}
Provides:		steamos.script = %{version}-%{release}
Provides:		org.valve.steamos.policy = %{version}-%{release}
Provides:		org.valve.steamos.rules = %{version}-%{release}
Provides:		steamos-cursor-config = %{version}-%{release}
Provides:		steamos-cursor.png = %{version}-%{release}
Provides:		steamos.png = %{version}-%{release}

%global _missing_build_ids_terminate_build 0
%global debug_package %{nil}

%description
%{summary}

%prep
curl --location --remote-time --show-error --fail --output %{SOURCE0} https://gitlab.com/evlaV/jupiter-hw-support/-/archive/master/jupiter-hw-support-master.tar.gz

%setup -q -c -n %{name}-master

%patch0 -p0

#%build
#(cd %{name}-master/usr/share/jupiter_bios_updater/driver/ && make)

%install
mkdir -p %{buildroot}/etc/default/
install -m 0644 %{name}-master/etc/default/grub-steamos %{buildroot}/etc/default/grub-steamos

mkdir -p %{buildroot}/etc/systemd/system/
install -m 0644 %{name}-master/etc/systemd/system/alsa-restore.service %{buildroot}/etc/systemd/system/alsa-restore.service
install -m 0644 %{name}-master/etc/systemd/system/steamos-automount@.service %{buildroot}/etc/systemd/system/steamos-automount@.service

mkdir -p %{buildroot}/usr/lib/systemd/system/
install -m 0644 %{name}-master/usr/lib/systemd/system/jupiter-biosupdate.service %{buildroot}/usr/lib/systemd/system/jupiter-biosupdate.service
install -m 0644 %{name}-master/usr/lib/systemd/system/jupiter-controller-update.service %{buildroot}/usr/lib/systemd/system/jupiter-controller-update.service

mkdir -p %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/
ln -sv /usr/lib/systemd/system/jupiter-biosupdate.service %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/
ln -sv /usr/lib/systemd/system/jupiter-controller-update.service %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/
ln -sv /usr/lib/systemd/system/alsa-restore.service %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/
ln -sv /usr/lib/systemd/system/steamos-automount@.service %{buildroot}/usr/lib/systemd/system/multi-user.target.wants/

mkdir -p %{buildroot}/etc/xdg/
install -m 0644 %{name}-master/etc/xdg/kded5rc %{buildroot}/etc/xdg/kded5rc

mkdir -p %{buildroot}/usr/bin
install -m 0755 %{name}-master/usr/bin/amd_system_info %{buildroot}/usr/bin/amd_system_info
install -m 0755 %{name}-master/usr/bin/foxnet-biosupdate %{buildroot}/usr/bin/foxnet-biosupdate
install -m 0755 %{name}-master/usr/bin/jupiter-biosupdate %{buildroot}/usr/bin/jupiter-biosupdate
install -m 0755 %{name}-master/usr/bin/jupiter-check-support %{buildroot}/usr/bin/jupiter-check-support
install -m 0755 %{name}-master/usr/bin/jupiter-controller-update %{buildroot}/usr/bin/jupiter-controller-update
install -m 0755 %{name}-master/usr/bin/thumbstick_cal %{buildroot}/usr/bin/thumbstick_cal
install -m 0755 %{name}-master/usr/bin/thumbstick_fine_cal %{buildroot}/usr/bin/thumbstick_fine_cal
install -m 0755 %{name}-master/usr/bin/trigger_cal %{buildroot}/usr/bin/trigger_cal

mkdir -p %{buildroot}/usr/bin/steamos-polkit-helpers/
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/jupiter-amp-control %{buildroot}/usr/bin/steamos-polkit-helpers/jupiter-amp-control
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/jupiter-biosupdate %{buildroot}/usr/bin/steamos-polkit-helpers/jupiter-biosupdate
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/jupiter-check-support %{buildroot}/usr/bin/steamos-polkit-helpers/jupiter-check-support
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/jupiter-dock-updater %{buildroot}/usr/bin/steamos-polkit-helpers/jupiter-dock-updater
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/jupiter-fan-control %{buildroot}/usr/bin/steamos-polkit-helpers/jupiter-fan-control
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/jupiter-get-als-gain %{buildroot}/usr/bin/steamos-polkit-helpers/jupiter-get-als-gain
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-devkit-mode %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-devkit-mode
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-disable-wireless-power-management %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-disable-wireless-power-management
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-enable-sshd %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-enable-sshd
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-factory-reset-config %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-factory-reset-config
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-format-device %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-format-device
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-format-sdcard %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-format-sdcard
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-poweroff-now %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-poweroff-now
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-priv-write %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-priv-write
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-reboot-now %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-reboot-now
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-reboot-other %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-reboot-other
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-restart-sddm %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-restart-sddm
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-select-branch %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-select-branch
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-set-hostname %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-set-hostname
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-set-timezone %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-set-timezone
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-trim-devices %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-trim-devices
install -m 0755 %{name}-master/usr/bin/steamos-polkit-helpers/steamos-update %{buildroot}/usr/bin/steamos-polkit-helpers/steamos-update

mkdir -p %{buildroot}/usr/lib/hwsupport/
install -m 0755 %{name}-master/usr/lib/hwsupport/cirrus-fixup.sh %{buildroot}/usr/lib/hwsupport/cirrus-fixup.sh
install -m 0755 %{name}-master/usr/lib/hwsupport/cs35l41-dsp1-spk-prot.bin.mod %{buildroot}/usr/lib/hwsupport/cs35l41-dsp1-spk-prot.bin.mod
install -m 0755 %{name}-master/usr/lib/hwsupport/cs35l41-dsp1-spk-prot.bin.orig %{buildroot}/usr/lib/hwsupport/cs35l41-dsp1-spk-prot.bin.orig
install -m 0755 %{name}-master/usr/lib/hwsupport/ev2_cirrus_alsa_fixups.sh %{buildroot}/usr/lib/hwsupport/ev2_cirrus_alsa_fixups.sh
install -m 0755 %{name}-master/usr/lib/hwsupport/format-device.sh %{buildroot}/usr/lib/hwsupport/format-device.sh
install -m 0755 %{name}-master/usr/lib/hwsupport/format-sdcard.sh %{buildroot}/usr/lib/hwsupport/format-sdcard.sh
install -m 0755 %{name}-master/usr/lib/hwsupport/jupiter-amp-control %{buildroot}/usr/lib/hwsupport/jupiter-amp-control
install -m 0755 %{name}-master/usr/lib/hwsupport/power-button-handler.py %{buildroot}/usr/lib/hwsupport/power-button-handler.py
install -m 0755 %{name}-master/usr/lib/hwsupport/steamos-automount.sh %{buildroot}/usr/lib/hwsupport/steamos-automount.sh
install -m 0755 %{name}-master/usr/lib/hwsupport/trim-devices.sh %{buildroot}/usr/lib/hwsupport/trim-devices.sh

#mkdir -p %{buildroot}/usr/lib/insyde/driver/
#install -m 0644 %{name}-master/usr/share/jupiter_bios_updater/driver/phy_alloc.ko %{buildroot}/usr/lib/insyde/driver/phy_alloc.ko

mkdir -p %{buildroot}/usr/lib/udev/rules.d/
install -m 0644 %{name}-master/usr/lib/udev/rules.d/80-gpu-reset.rules %{buildroot}/usr/lib/udev/rules.d/80-gpu-reset.rules
install -m 0644 %{name}-master/usr/lib/udev/rules.d/99-power-button.rules %{buildroot}/usr/lib/udev/rules.d/99-power-button.rules
install -m 0644 %{name}-master/usr/lib/udev/rules.d/99-steamos-automount.rules %{buildroot}/usr/lib/udev/rules.d/99-steamos-automount.rules

mkdir -p %{buildroot}/usr/share/alsa/ucm2/conf.d/acp5x/
install -m 0644 %{name}-master/usr/share/alsa/ucm2/conf.d/acp5x/HiFi-upstream.conf %{buildroot}/usr/share/alsa/ucm2/conf.d/acp5x/HiFi-upstream.conf
install -m 0644 %{name}-master/usr/share/alsa/ucm2/conf.d/acp5x/HiFi.conf %{buildroot}/usr/share/alsa/ucm2/conf.d/acp5x/HiFi.conf
install -m 0644 %{name}-master/usr/share/alsa/ucm2/conf.d/acp5x/acp5x.conf %{buildroot}/usr/share/alsa/ucm2/conf.d/acp5x/acp5x.conf

mkdir -p %{buildroot}/usr/share/icons/steam/cursors/
ln -sv %{name}-master/usr/share/steamos/steamos-cursor.png ./; xcursorgen %{name}-master/usr/share/steamos/steamos-cursor-config %{buildroot}/usr/share/icons/steam/cursors/default
ln -sv /usr/share/icons/steam/cursors/default %{buildroot}/usr/share/icons/steam/cursors/arrow
ln -sv /usr/share/icons/steam/cursors/default %{buildroot}/usr/share/icons/steam/cursors/left_ptr
ln -sv /usr/share/icons/steam/cursors/default %{buildroot}/usr/share/icons/steam/cursors/left_ptr_help
ln -sv /usr/share/icons/steam/cursors/default %{buildroot}/usr/share/icons/steam/cursors/left_ptr_watch

mkdir -p %{buildroot}/usr/share/icons/steam/
install -m 0644 %{name}-master/usr/share/icons/steam/index.theme %{buildroot}/usr/share/icons/steam/index.theme

mkdir -p %{buildroot}/usr/share/jupiter_bios
install -m 0644 %{name}-master/usr/share/jupiter_bios/F7A0113_sign.fd %{buildroot}/usr/share/jupiter_bios/F7A0113_sign.fd

mkdir -p %{buildroot}/usr/share/jupiter_bios_updater/
install -m 0755 %{name}-master/usr/share/jupiter_bios_updater/BatCtrl %{buildroot}/usr/share/jupiter_bios_updater/BatCtrl
install -m 0755 %{name}-master/usr/share/jupiter_bios_updater/H2OFFTx64-G.sh %{buildroot}/usr/share/jupiter_bios_updater/H2OFFTx64-G.sh
install -m 0755 %{name}-master/usr/share/jupiter_bios_updater/H2OFFTx64.sh %{buildroot}/usr/share/jupiter_bios_updater/H2OFFTx64.sh
install -m 0644 %{name}-master/usr/share/jupiter_bios_updater/Logo.png %{buildroot}/usr/share/jupiter_bios_updater/Logo.png
install -m 0755 %{name}-master/usr/share/jupiter_bios_updater/h2offt %{buildroot}/usr/share/jupiter_bios_updater/h2offt
install -m 0644 %{name}-master/usr/share/jupiter_bios_updater/h2offt-g %{buildroot}/usr/share/jupiter_bios_updater/h2offt-g
install -m 0755 %{name}-master/usr/share/jupiter_bios_updater/h2osde-lx64 %{buildroot}/usr/share/jupiter_bios_updater/h2osde-lx64
install -m 0644 %{name}-master/usr/share/jupiter_bios_updater/msg_cht.ini %{buildroot}/usr/share/jupiter_bios_updater/msg_cht.ini
install -m 0644 %{name}-master/usr/share/jupiter_bios_updater/msg_eng.ini %{buildroot}/usr/share/jupiter_bios_updater/msg_eng.ini
install -m 0644 %{name}-master/usr/share/jupiter_bios_updater/platform.ini %{buildroot}/usr/share/jupiter_bios_updater/platform.ini

mkdir -p %{buildroot}/usr/share/jupiter_controller_fw_updater/
install -m 0644 %{name}-master/usr/share/jupiter_controller_fw_updater/D20_APP_REL_63D05208.bin %{buildroot}/usr/share/jupiter_controller_fw_updater/D20_APP_REL_63D05208.bin
install -m 0644 %{name}-master/usr/share/jupiter_controller_fw_updater/D21_APP_REL_63D051FB.bin %{buildroot}/usr/share/jupiter_controller_fw_updater/D21_APP_REL_63D051FB.bin
install -m 0644 %{name}-master/usr/share/jupiter_controller_fw_updater/RA_APP_REL_63D05207.bin %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_APP_REL_63D05207.bin
install -m 0644 %{name}-master/usr/share/jupiter_controller_fw_updater/EV2_REL_62C853C3.bin %{buildroot}/usr/share/jupiter_controller_fw_updater/EV2_REL_62C853C3.bin
install -m 0755 %{name}-master/usr/share/jupiter_controller_fw_updater/d20bootloader.py %{buildroot}/usr/share/jupiter_controller_fw_updater/d20bootloader.py
install -m 0755 %{name}-master/usr/share/jupiter_controller_fw_updater/d21bootloader16.py %{buildroot}/usr/share/jupiter_controller_fw_updater/d21bootloader16.py

mkdir -p %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/
install -m 0644 %{name}-master/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/boot_ra_Release.srec %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/boot_ra_Release.srec
install -m 0755 %{name}-master/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/rfp_cli_linux.sh %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/rfp_cli_linux.sh

mkdir -p %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/
install -m 0755 %{name}-master/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/BatCtrl %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/BatCtrl

mkdir -p %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/
install -m 0644 %{name}-master/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/Devices.xml %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/Devices.xml
install -m 0644 %{name}-master/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/License_Agreement.txt %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/License_Agreement.txt
install -m 0644 %{name}-master/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/Messages.xml %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/Messages.xml
install -m 0644 %{name}-master/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/libRFP.so %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/libRFP.so
install -m 0755 %{name}-master/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/rfp-cli %{buildroot}/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/rfp-cli

mkdir -p %{buildroot}/usr/share/plymouth/themes/steamos/
install -m 0644 %{name}-master/usr/share/plymouth/themes/steamos/steamos.plymouth %{buildroot}/usr/share/plymouth/themes/steamos/steamos.plymouth
install -m 0644 %{name}-master/usr/share/plymouth/themes/steamos/steamos.png %{buildroot}/usr/share/plymouth/themes/steamos/steamos.png
install -m 0644 %{name}-master/usr/share/plymouth/themes/steamos/steamos.script %{buildroot}/usr/share/plymouth/themes/steamos/steamos.script

mkdir -p %{buildroot}/usr/share/polkit-1/actions/
install -m 0644 %{name}-master/usr/share/polkit-1/actions/org.valve.steamos.policy %{buildroot}/usr/share/polkit-1/actions/org.valve.steamos.policy

mkdir -p %{buildroot}/usr/share/polkit-1/rules.d/
install -m 0644 %{name}-master/usr/share/polkit-1/rules.d/org.valve.steamos.rules %{buildroot}/usr/share/polkit-1/rules.d/org.valve.steamos.rules

mkdir -p %{buildroot}/usr/share/steamos/
install -m 0644 %{name}-master/usr/share/steamos/steamos-cursor-config %{buildroot}/usr/share/steamos/steamos-cursor-config
install -m 0644 %{name}-master/usr/share/steamos/steamos-cursor.png %{buildroot}/usr/share/steamos/steamos-cursor.png
install -m 0644 %{name}-master/usr/share/steamos/steamos.png %{buildroot}/usr/share/steamos/steamos.png

%files
%defattr(-,root,root,-)
/etc/default/grub-steamos
/etc/systemd/system/alsa-restore.service
/etc/systemd/system/steamos-automount@.service
/usr/lib/systemd/system/jupiter-biosupdate.service
/usr/lib/systemd/system/jupiter-controller-update.service
/usr/lib/systemd/system/multi-user.target.wants/jupiter-biosupdate.service
/usr/lib/systemd/system/multi-user.target.wants/jupiter-controller-update.service
/usr/lib/systemd/system/multi-user.target.wants/alsa-restore.service
/usr/lib/systemd/system/multi-user.target.wants/steamos-automount@.service
/etc/xdg/kded5rc
/usr/bin/amd_system_info
/usr/bin/foxnet-biosupdate
/usr/bin/jupiter-biosupdate
/usr/bin/jupiter-check-support
/usr/bin/jupiter-controller-update
/usr/bin/thumbstick_cal
/usr/bin/thumbstick_fine_cal
/usr/bin/trigger_cal
/usr/bin/steamos-polkit-helpers/jupiter-amp-control
/usr/bin/steamos-polkit-helpers/jupiter-biosupdate
/usr/bin/steamos-polkit-helpers/jupiter-check-support
/usr/bin/steamos-polkit-helpers/jupiter-dock-updater
/usr/bin/steamos-polkit-helpers/jupiter-fan-control
/usr/bin/steamos-polkit-helpers/jupiter-get-als-gain
/usr/bin/steamos-polkit-helpers/steamos-devkit-mode
/usr/bin/steamos-polkit-helpers/steamos-disable-wireless-power-management
/usr/bin/steamos-polkit-helpers/steamos-enable-sshd
/usr/bin/steamos-polkit-helpers/steamos-factory-reset-config
/usr/bin/steamos-polkit-helpers/steamos-format-device
/usr/bin/steamos-polkit-helpers/steamos-format-sdcard
/usr/bin/steamos-polkit-helpers/steamos-poweroff-now
/usr/bin/steamos-polkit-helpers/steamos-priv-write
/usr/bin/steamos-polkit-helpers/steamos-reboot-now
/usr/bin/steamos-polkit-helpers/steamos-reboot-other
/usr/bin/steamos-polkit-helpers/steamos-restart-sddm
/usr/bin/steamos-polkit-helpers/steamos-select-branch
/usr/bin/steamos-polkit-helpers/steamos-set-hostname
/usr/bin/steamos-polkit-helpers/steamos-set-timezone
/usr/bin/steamos-polkit-helpers/steamos-trim-devices
/usr/bin/steamos-polkit-helpers/steamos-update
/usr/lib/hwsupport/cirrus-fixup.sh
/usr/lib/hwsupport/cs35l41-dsp1-spk-prot.bin.mod
/usr/lib/hwsupport/cs35l41-dsp1-spk-prot.bin.orig
/usr/lib/hwsupport/ev2_cirrus_alsa_fixups.sh
/usr/lib/hwsupport/format-device.sh
/usr/lib/hwsupport/format-sdcard.sh
/usr/lib/hwsupport/jupiter-amp-control
/usr/lib/hwsupport/power-button-handler.py
/usr/lib/hwsupport/steamos-automount.sh
/usr/lib/hwsupport/trim-devices.sh
/usr/lib/udev/rules.d/80-gpu-reset.rules
/usr/lib/udev/rules.d/99-power-button.rules
/usr/lib/udev/rules.d/99-steamos-automount.rules
#/usr/lib/insyde/driver/phy_alloc.ko
/usr/share/alsa/ucm2/conf.d/acp5x/HiFi-upstream.conf
/usr/share/alsa/ucm2/conf.d/acp5x/HiFi.conf
/usr/share/alsa/ucm2/conf.d/acp5x/acp5x.conf
/usr/share/icons/steam/cursors/default
/usr/share/icons/steam/cursors/arrow
/usr/share/icons/steam/cursors/left_ptr
/usr/share/icons/steam/cursors/left_ptr_help
/usr/share/icons/steam/cursors/left_ptr_watch
/usr/share/icons/steam/index.theme
/usr/share/jupiter_bios/F7A0113_sign.fd
/usr/share/jupiter_bios_updater/BatCtrl
/usr/share/jupiter_bios_updater/H2OFFTx64-G.sh
/usr/share/jupiter_bios_updater/H2OFFTx64.sh
/usr/share/jupiter_bios_updater/Logo.png
/usr/share/jupiter_bios_updater/h2offt
/usr/share/jupiter_bios_updater/h2offt-g
/usr/share/jupiter_bios_updater/h2osde-lx64
/usr/share/jupiter_bios_updater/msg_cht.ini
/usr/share/jupiter_bios_updater/msg_eng.ini
/usr/share/jupiter_bios_updater/platform.ini
/usr/share/jupiter_controller_fw_updater/D20_APP_REL_63D05208.bin
/usr/share/jupiter_controller_fw_updater/D21_APP_REL_63D051FB.bin
/usr/share/jupiter_controller_fw_updater/RA_APP_REL_63D05207.bin
/usr/share/jupiter_controller_fw_updater/EV2_REL_62C853C3.bin
/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/boot_ra_Release.srec
/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/BatCtrl
/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/Devices.xml
/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/License_Agreement.txt
/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/Messages.xml
/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/libRFP.so
/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/linux_host_tools/rfp-linux-x64/rfp-cli
/usr/share/jupiter_controller_fw_updater/RA_bootloader_updater/rfp_cli_linux.sh
/usr/share/jupiter_controller_fw_updater/d20bootloader.py
/usr/share/jupiter_controller_fw_updater/d21bootloader16.py
/usr/share/plymouth/themes/steamos/steamos.plymouth
/usr/share/plymouth/themes/steamos/steamos.png
/usr/share/plymouth/themes/steamos/steamos.script
/usr/share/polkit-1/actions/org.valve.steamos.policy
/usr/share/polkit-1/rules.d/org.valve.steamos.rules
/usr/share/steamos/steamos-cursor-config
/usr/share/steamos/steamos-cursor.png
/usr/share/steamos/steamos.png

%changelog
* Sun Feb 12 2023 Karlis Kavacis karlis.kavacis@protonmail.com
- Update to 20230201

* Tue Dec 20 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Update to 20221213.1

* Mon Nov 28 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Version bump to 20221121.1, new dependencies added, new jupiter_controller_fw_updater bin versions added

* Mon Nov 07 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build based on Arch PKGBUILD sources
