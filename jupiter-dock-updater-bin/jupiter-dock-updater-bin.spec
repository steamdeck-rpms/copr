Name:			jupiter-dock-updater-bin
Version:		20230126.01
Release:		1%{?dist}
Summary:		"Firmware updater for the Steam Deck Dock."

License:		proprietary
URL:			https://gitlab.steamos.cloud/jupiter/jupiter-dock-updater-bin
Source0:		https://gitlab.com/evlaV/jupiter-dock-updater-bin/-/archive/main/jupiter-dock-updater-bin-main.tar.gz

BuildRequires:  curl tar

Provides:		hub_update = %{version}-%{release}
Provides:		jaguar-b0_LUXSHARE_spi_image_V0.013.15.0.119_230125.bin = %{version}-%{release}
Provides:		jaguar-b0_mca_i2c_isp_driver_payload.bin = %{version}-%{release}
Provides:		jupiter-dock-updater-mock.sh = %{version}-%{release}
Provides:		jupiter-dock-updater.sh = %{version}-%{release}
Provides:		update.ini = %{version}-%{release}

%description
%{summary}

%prep
curl --location --remote-time --show-error --fail --output %{SOURCE0} https://gitlab.com/evlaV/jupiter-dock-updater-bin/-/archive/main/jupiter-dock-updater-bin-main.tar.gz

%setup -q -c -n %{name}-main

%install
mkdir -p %{buildroot}/usr/lib/jupiter-dock-updater/
install -m 0755 %{name}-main/packaged/usr/lib/jupiter-dock-updater/hub_update %{buildroot}/usr/lib/jupiter-dock-updater/hub_update
install -m 0755 %{name}-main/packaged/usr/lib/jupiter-dock-updater/jaguar-b0_LUXSHARE_spi_image_V0.013.15.0.119_230125.bin %{buildroot}/usr/lib/jupiter-dock-updater/jaguar-b0_LUXSHARE_spi_image_V0.013.15.0.119_230125.bin
install -m 0755 %{name}-main/packaged/usr/lib/jupiter-dock-updater/jaguar-b0_mca_i2c_isp_driver_payload.bin %{buildroot}/usr/lib/jupiter-dock-updater/jaguar-b0_mca_i2c_isp_driver_payload.bin
install -m 0755 %{name}-main/packaged/usr/lib/jupiter-dock-updater/jupiter-dock-updater-mock.sh %{buildroot}/usr/lib/jupiter-dock-updater/jupiter-dock-updater-mock.sh
install -m 0755 %{name}-main/packaged/usr/lib/jupiter-dock-updater/jupiter-dock-updater.sh %{buildroot}/usr/lib/jupiter-dock-updater/jupiter-dock-updater.sh
install -m 0644 %{name}-main/packaged/usr/lib/jupiter-dock-updater/update.ini %{buildroot}/usr/lib/jupiter-dock-updater/update.ini

%files
%defattr(-,root,root,-)
/usr/lib/jupiter-dock-updater/hub_update
/usr/lib/jupiter-dock-updater/jaguar-b0_LUXSHARE_spi_image_V0.013.15.0.119_230125.bin
/usr/lib/jupiter-dock-updater/jaguar-b0_mca_i2c_isp_driver_payload.bin
/usr/lib/jupiter-dock-updater/jupiter-dock-updater-mock.sh
/usr/lib/jupiter-dock-updater/jupiter-dock-updater.sh
/usr/lib/jupiter-dock-updater/update.ini

%changelog
* Sun Feb 12 2023 Karlis Kavacis karlis.kavacis@protonmail.com
- Update to 20230126.01

* Mon Nov 07 2022 Karlis Kavacis karlis.kavacis@protonmail.com
- Initial RPM Build based on Arch PKGBUILD sources
